#ifndef TRIANGLERENDERER_HPP
#define TRIANGLERENDERER_HPP

#include "shaderprogram.hpp"
#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class TriangleRenderer : public QObject
{
  Q_OBJECT
public:
  explicit TriangleRenderer(QObject* parent = nullptr);
  void initialize();
  void render();

private:
  const GLfloat VERTICES[9];

  QOpenGLFunctions* functions;
  ShaderProgram shaderProgram;

  /**
   * @brief initializeShaderProgram Load the shaders and get a linked program
   * object
   */
  void initializeShaderProgram();
};

#endif // TRIANGLERENDERER_HPP
