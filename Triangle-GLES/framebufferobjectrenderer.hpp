#ifndef FRAMEBUFFEROBJECTRENDERER_H
#define FRAMEBUFFEROBJECTRENDERER_H

#include "trianglerenderer.hpp"
#include <QQuickFramebufferObject>
#include <QQuickWindow>

class FrameBufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
public:
  FrameBufferObjectRenderer();
  void synchronize(QQuickFramebufferObject* item) override;
  void render() override;
  QOpenGLFramebufferObject* createFramebufferObject(const QSize& size) override;

private:
  QQuickWindow* quickWindow;
  TriangleRenderer triangleRenderer;
};

#endif // FRAMEBUFFEROBJECTRENDERER_H
