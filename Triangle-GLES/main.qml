import QtQuick 2.10
import QtQuick.Controls 2.2
import FrameBufferObject 1.0

ApplicationWindow {
  visible: true
  width: 640
  height: 480
  title: qsTr("Hello World - OpenGL ES 3.1 [Triangle]")

  Rectangle {
    width: parent.width - 20
    height: parent.height - 20
    border.color: "black"
    border.width: 1
    anchors {
      horizontalCenter: parent.horizontalCenter
      verticalCenter: parent.verticalCenter
    }

    Rectangle {
      width: parent.width - 20
      height: parent.height - 20
      anchors {
        horizontalCenter: parent.horizontalCenter
        verticalCenter: parent.verticalCenter
      }

      FbItem {
        anchors.fill: parent
      }
    }
  }
}
