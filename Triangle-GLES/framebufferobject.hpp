#ifndef FRAMEBUFFEROBJECT_H
#define FRAMEBUFFEROBJECT_H

#include "framebufferobjectrenderer.hpp"
#include <QQuickFramebufferObject>

class FrameBufferObject : public QQuickFramebufferObject
{
  Q_OBJECT

public:
  explicit FrameBufferObject(QQuickItem* parent = 0);
  Renderer* createRenderer() const override;
};

#endif // FRAMEBUFFEROBJECT_H
