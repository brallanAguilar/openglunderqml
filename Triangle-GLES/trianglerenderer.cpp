#include "trianglerenderer.hpp"
#include <QOpenGLContext>
#include <iostream>

TriangleRenderer::TriangleRenderer(QObject* parent)
  : QObject{ parent }
  , VERTICES{ 0.0f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f }
  , functions{ QOpenGLContext::currentContext()->functions() }
{
}

void
TriangleRenderer::initialize()
{
  try {
    initializeShaderProgram();
  } catch (const std::runtime_error& ex) {
    std::cerr << ex.what() << "\nProgram was not initialized" << std::endl;
  }
}

void
TriangleRenderer::render()
{
  // Set the viewport
  functions->glViewport(0, 0, 320, 240);

  // Clear the color buffer
  functions->glClear(GL_COLOR_BUFFER_BIT);

  // Use the program object
  functions->glUseProgram(shaderProgram.id());

  // 1st attribute buffer: vertices
  functions->glEnableVertexAttribArray(0);

  // Load the vertex position
  functions->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, VERTICES);

  // Starting from vertex 0; 3 vertices total -> 1 triangle
  functions->glDrawArrays(GL_TRIANGLES, 0, 3);
}

void
TriangleRenderer::initializeShaderProgram()
{
  shaderProgram.create();
  shaderProgram.loadShaders();
  shaderProgram.link();
}
