#include "framebufferobject.hpp"

FrameBufferObject::FrameBufferObject(QQuickItem* parent)
  : QQuickFramebufferObject(parent)
  , m_rotationY{ 0 }
{
  setMirrorVertically(true);
}

void
FrameBufferObject::addPoint()
{
  m_pointClicked.first = m_mousePosition;
  m_pointClicked.second = true;
  update();
}

void
FrameBufferObject::unprojectPosition(QVector2D position)
{
  m_unprojectPoint = position;
  update();
}

QQuickFramebufferObject::Renderer*
FrameBufferObject::createRenderer() const
{
  return new FrameBufferObjectRenderer();
}

const QVector3D&
FrameBufferObject::pointClicked()
{
  return m_pointClicked.first;
}

bool
FrameBufferObject::hasPoints() const
{
  return m_pointClicked.second;
}

void
FrameBufferObject::pointProcessed()
{
  m_pointClicked.second = false;
}

void
FrameBufferObject::setUnprojectPoint(QVector3D point)
{
  m_mousePosition = point;
}

const QVector3D&
FrameBufferObject::getUnprojectPoint() const
{
  return m_unprojectPoint;
}

const int&
FrameBufferObject::rotationY() const
{
  return m_rotationY;
}

const QVector3D
FrameBufferObject::mousePosition() const
{
  return m_mousePosition;
}

void
FrameBufferObject::setRotationY(int angle)
{
  if (m_rotationY != angle) {
    m_rotationY = angle;
    emit rotationYChanged(angle);
    update();
  }
}

void
FrameBufferObject::setMousePosition(const QVector3D& position)
{
  if (m_mousePosition != position) {
    m_mousePosition = position;
    emit mousePositionChanged(position);
    update();
  }
}
