import QtQuick.Controls 2.2
import QtQuick 2.9
import FrameBufferObject 1.0

Item {
  Rectangle {
    anchors.fill: parent
    anchors {
      horizontalCenter: parent.horizontalCenter
      verticalCenter: parent.verticalCenter
    }

    FbItem {
      id: fbo
      anchors.fill: parent
      rotationY: panel.rotationScale
      MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
          fbo.addPoint(mouseX, mouseY)
        }
        onPositionChanged: {
          fbo.unprojectPosition(Qt.vector2d(mouseX, mouseY))
          lblCoords.points = fbo.mousePosition
        }
      }
      Label {
        text: "Rotation Y:" + fbo.formatNumber(panel.rotationScale, 0) + "°"
        anchors.right: fbo.right
        anchors.bottom: fbo.bottom
        color: "white"
      }
      Label {
        id: lblCoords
        property vector3d points
        text: "x:" + fbo.formatNumber(points.x, 3) + " y:" + fbo.formatNumber(
                points.y, 3) + " z:" + fbo.formatNumber(points.z, 3)
        anchors.left: fbo.left
        anchors.bottom: fbo.bottom
        color: "white"
      }
      function formatNumber(number, precision) {
        return number.toFixed(precision)
      }
    }
  }
}
