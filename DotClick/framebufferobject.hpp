#ifndef FRAMEBUFFEROBJECT_HPP
#define FRAMEBUFFEROBJECT_HPP

#include "framebufferobjectrenderer.hpp"
#include <QPair>
#include <QQuickFramebufferObject>
#include <QVector3D>

class FrameBufferObject : public QQuickFramebufferObject
{
  Q_OBJECT
  Q_PROPERTY(
    int rotationY READ rotationY WRITE setRotationY NOTIFY rotationYChanged)
  Q_PROPERTY(QVector3D mousePosition READ mousePosition WRITE setMousePosition
               NOTIFY mousePositionChanged)

public:
  explicit FrameBufferObject(QQuickItem* parent = 0);
  Q_INVOKABLE void addPoint();
  Q_INVOKABLE void unprojectPosition(QVector2D position);
  Renderer* createRenderer() const override;
  const QVector3D& pointClicked();
  bool hasPoints() const;
  void pointProcessed();
  void setUnprojectPoint(QVector3D point);
  const QVector3D& getUnprojectPoint() const;
  const int& rotationY() const;
  const QVector3D mousePosition() const;

signals:
  void rotationYChanged(int angle);
  void mousePositionChanged(const QVector3D& position);

public slots:
  void setRotationY(int angle);
  void setMousePosition(const QVector3D& position);

private:
  int m_rotationY;
  QVector3D m_mousePosition;
  QVector3D m_unprojectPoint;
  QPair<QVector3D, bool> m_pointClicked;
};

#endif // FRAMEBUFFEROBJECT_HPP
