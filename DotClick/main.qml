import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
  visible: true
  width: 800
  height: 600
  title: qsTr("Hello World - OpenGL 3.3 [DotClick]")

  GridLayout {
    columns: 2
    anchors.fill: parent
    OpenGLCanvas {
      id: canvas
      width: parent.width - 300
      height: parent.height
    }
    Panel {
      id: panel
      Layout.fillWidth: true
      height: parent.height
      anchors.left: canvas.right
    }
  }
}
