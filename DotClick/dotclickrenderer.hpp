#ifndef DOTCLICKRENDERER_HPP
#define DOTCLICKRENDERER_HPP

#include <QMatrix4x4>
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QVector3D>

class DotClickRenderer : public QObject
{
  Q_OBJECT
public:
  explicit DotClickRenderer(QObject* parent = nullptr);
  ~DotClickRenderer();
  void initialize();
  void render();
  void appendVertex(const QVector3D& point);
  void setViewportSize(const QSize& size);
  void setRotationY(int angle);

  QVector3D unprojectPoint(const QVector3D& vector);

private:
  const QString VERTEX_SHADER_FILENAME;
  const QString FRAGMENT_SHADER_FILENAME;

  QOpenGLShaderProgram shaderProgram;
  QVector<QVector3D> vertices;
  QOpenGLFunctions* functions;
  QOpenGLVertexArrayObject vao;
  QOpenGLBuffer vbo;
  QSize viewportSize;
  int rotationY;

  QMatrix4x4 mvp;
  const QMatrix4x4 modelView;
  QMatrix4x4 projection;

  void initializeShaderProgram();
  void initializeVao();
  void initializeVbo();
  void initializeMatrices();
};

#endif // DOTCLICKRENDERER_HPP
