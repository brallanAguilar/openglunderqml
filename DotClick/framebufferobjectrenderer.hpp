#ifndef FRAMEBUFFEROBJECTRENDERER_HPP
#define FRAMEBUFFEROBJECTRENDERER_HPP

#include "dotclickrenderer.hpp"
#include "framebufferobject.hpp"
#include <QQuickFramebufferObject>
#include <QQuickWindow>

class FrameBufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
public:
  FrameBufferObjectRenderer();
  void synchronize(QQuickFramebufferObject* item) override;
  void render() override;
  QOpenGLFramebufferObject* createFramebufferObject(const QSize& size) override;

private:
  QQuickWindow* quickWindow;
  DotClickRenderer dotRenderer;
  void updateRenderer(QQuickFramebufferObject* item);

  /**
   * @brief rotationY value of rotationY and if it is different to a previous
   * value
   */
  QPair<float, bool> rotationY;
  bool redraw;
  void pointAdded(QQuickFramebufferObject* item);
  void changedRotationY(QQuickFramebufferObject* item);
  void unprojectMouseCoordinates(QQuickFramebufferObject* item);
};

#endif // FRAMEBUFFEROBJECTRENDERER_HPP
