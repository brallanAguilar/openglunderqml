import QtQuick.Controls 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.3

Item {
  property real rotationScale

  GridLayout {
    columns: 2
    Label {
      text: "Rotation Y"
    }
    Slider {
      id: slider
      from: -90
      to: 90
      value: 0
      onMoved: {
        rotationScale = value
      }
    }
    Button {
      text: "Reset"
      onClicked: {
        slider.value = 0
        rotationScale = 0
      }
    }
  }
}
