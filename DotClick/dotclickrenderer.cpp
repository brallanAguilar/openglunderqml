#include "dotclickrenderer.hpp"
#include <QOpenGLContext>
#include <iostream>

DotClickRenderer::DotClickRenderer(QObject* parent)
  : QObject{ parent }
  , VERTEX_SHADER_FILENAME{ "shader.vert" }
  , FRAGMENT_SHADER_FILENAME{ "shader.frag" }
  , functions{ QOpenGLContext::currentContext()->functions() }
  , vbo{ QOpenGLBuffer::VertexBuffer }
  , rotationY{ 0 }
{
}

DotClickRenderer::~DotClickRenderer()
{
  vbo.destroy();
  vao.destroy();
}

void
DotClickRenderer::initialize()
{
  initializeShaderProgram();
  initializeVao();
  initializeVbo();
  projection.perspective(0, viewportSize.width() / viewportSize.height(), 1, 1);

  // Allows to use the gl_PointSize on the vertex shader
  functions->glEnable(GL_PROGRAM_POINT_SIZE);
}

void
DotClickRenderer::render()
{
  // Set black background
  functions->glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  // Clear the color, depth and stencil buffer
  functions->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                     GL_STENCIL_BUFFER_BIT);

  // Binds this shader program to the active QOpenGLContext and makes it the
  // current shader program
  shaderProgram.bind();

  initializeMatrices();

  // Binds this vertex array object to the OpenGL binding point. From this point
  // on and until release() is called or another vertex array object is bound,
  // any modifications made to vertex data state are stored inside this vertex
  // array object.
  // If another vertex array object is then bound you can later restore the set
  // of state associated with this object by calling bind() on this object once
  // again. This allows efficient changes between vertex data states in
  // rendering functions
  vao.bind();

  // Tell OpenGL which VBOs to use
  vbo.bind();

  vbo.allocate(vertices.constData(), sizeof(QVector3D) * vertices.size());

  // Tell OpenGL programmable pipeline how to locate vertex position data
  int vertexLocation{ shaderProgram.attributeLocation("position") };
  shaderProgram.enableAttributeArray(vertexLocation);
  shaderProgram.setAttributeBuffer(
    vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));

  functions->glDrawArrays(GL_POINTS, 0, vertices.size());

  // It is good practice to disable each vertex attribute when it is not
  // immediately used. Leaving it enabled when a shader is not using it is a
  // sure way of asking for trouble
  shaderProgram.disableAttributeArray(vertexLocation);

  vao.release();
  vbo.release();
  shaderProgram.release();
}

QVector3D
DotClickRenderer::unprojectPoint(const QVector3D& vector)
{
  QRect viewport{ { 0, 0 }, viewportSize };
  QVector3D mouseInViewport{ vector.unproject(mvp, projection, viewport) };
  mouseInViewport.setY(-mouseInViewport.y());

  return mouseInViewport;
}

void
DotClickRenderer::appendVertex(const QVector3D& point)
{
  vertices.append(point);
}

void
DotClickRenderer::setViewportSize(const QSize& size)
{
  viewportSize = size;
}

void
DotClickRenderer::setRotationY(int angle)
{
  rotationY = angle;
}

void
DotClickRenderer::initializeShaderProgram()
{
  shaderProgram.create();
  shaderProgram.addShaderFromSourceFile(
    QOpenGLShader::Vertex, QString(":/%1").arg(VERTEX_SHADER_FILENAME));
  shaderProgram.addShaderFromSourceFile(
    QOpenGLShader::Fragment, QString(":/%1").arg(FRAGMENT_SHADER_FILENAME));
  shaderProgram.link();
  shaderProgram.bind();
}

void
DotClickRenderer::initializeVao()
{
  vao.create();
  vao.bind();
  vao.release();
}

void
DotClickRenderer::initializeVbo()
{
  vbo.create();
  vbo.bind();
  vbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
  vbo.allocate(vertices.constData(), sizeof(QVector3D) * 1000);
  vbo.release();
}

void
DotClickRenderer::initializeMatrices()
{
  mvp.setToIdentity();
  mvp.rotate(rotationY, 0.0, 1.0, 0.0);
  shaderProgram.setUniformValue("mvp", mvp);
}
