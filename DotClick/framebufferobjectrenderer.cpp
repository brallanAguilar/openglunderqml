#include "framebufferobjectrenderer.hpp"
#include "framebufferobject.hpp"
#include <QOpenGLFramebufferObjectFormat>

FrameBufferObjectRenderer::FrameBufferObjectRenderer()
  : rotationY{ 0, false }
  , redraw{ true }
{
  dotRenderer.initialize();
}

void
FrameBufferObjectRenderer::synchronize(QQuickFramebufferObject* item)
{
  updateRenderer(item);
  unprojectMouseCoordinates(item);
  quickWindow = item->window();
}

void
FrameBufferObjectRenderer::render()
{
  if (redraw) {
    dotRenderer.render();
    redraw = false;
  }
  quickWindow->resetOpenGLState();
}

QOpenGLFramebufferObject*
FrameBufferObjectRenderer::createFramebufferObject(const QSize& size)
{
  QOpenGLFramebufferObjectFormat format;
  format.setSamples(4);
  format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
  dotRenderer.setViewportSize(size);
  redraw = true;
  return new QOpenGLFramebufferObject(size, format);
}

void
FrameBufferObjectRenderer::pointAdded(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  if (fbo->hasPoints()) {
    dotRenderer.appendVertex(fbo->pointClicked());
    fbo->pointProcessed();
    redraw = true;
  }
}

void
FrameBufferObjectRenderer::changedRotationY(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  rotationY.second = fbo->rotationY() != rotationY.first;
  if (rotationY.second) {
    dotRenderer.setRotationY(fbo->rotationY());
    rotationY.first = fbo->rotationY();
    redraw = true;
  }
}

void
FrameBufferObjectRenderer::updateRenderer(QQuickFramebufferObject* item)
{
  pointAdded(item);
  changedRotationY(item);
}

void
FrameBufferObjectRenderer::unprojectMouseCoordinates(
  QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  QVector3D newCoordinates{ dotRenderer.unprojectPoint(
    fbo->getUnprojectPoint()) };
  fbo->setUnprojectPoint(newCoordinates);
}
