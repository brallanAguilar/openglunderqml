import qbs

Product {
    type: ["application"]
    cpp.cxxLanguageVersion: "c++17"
    Depends {
        name: "Qt"
        submodules: ["core", "quick", "opengl", "qml"]
    }
    files: [
        "OpenGLCanvas.qml",
        "Panel.qml",
        "dotclickrenderer.cpp",
        "dotclickrenderer.hpp",
        "framebufferobject.cpp",
        "framebufferobject.hpp",
        "framebufferobjectrenderer.cpp",
        "framebufferobjectrenderer.hpp",
        "main.cpp",
        "main.qml",
        "qml.qrc",
        "shader.frag",
        "shader.vert",
    ]
}
