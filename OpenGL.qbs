import qbs

Project {
    references: [
        "BackgroundColor",
        "Dot",
        "DotClick",
        "Texture2D-2x2Image-GL",
        "Texture2D-2x2Image-GLES",
        "Triangle-Camera",
        "Triangle-GL",
        "Triangle-GLES",
    ]
}
