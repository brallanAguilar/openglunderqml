#ifndef DOTRENDERER_HPP
#define DOTRENDERER_HPP

#include "shaderprogram.hpp"
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

class DotRenderer : public QObject
{
  Q_OBJECT
public:
  explicit DotRenderer(QObject* parent = nullptr);
  ~DotRenderer();
  void initialize();
  void render();

private:
  const QVector<QVector3D> VERTICES;

  QOpenGLFunctions* functions;
  ShaderProgram shaderProgram;
  QOpenGLVertexArrayObject vao;
  QOpenGLBuffer vbo;

  /**
   * @brief initializeShaderProgram Load the shaders and get a linked program
   * object
   */
  void initializeShaderProgram();

  void initializeVao();
  void initializeVbo();
};

#endif // DOTRENDERER_HPP
