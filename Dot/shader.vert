#version 330 core

layout(location = 0) in vec4 vertexPosition_modelspace;

void main()
{
  // write the size of the point sprite in pixels
  gl_PointSize = 4.0;
  gl_Position = vertexPosition_modelspace;
}
