#include "shaderprogram.hpp"

ShaderProgram::ShaderProgram()
  : shaderProgram{ new QOpenGLShaderProgram() }
{
}

ShaderProgram::~ShaderProgram()
{
  delete shaderProgram;
}

void
ShaderProgram::create()
{
  if (!shaderProgram->create()) {
    throw std::runtime_error("Shader program was not created");
  }
}

void
ShaderProgram::loadShaders()
{
  loadShaderFromResources(QOpenGLShader::Vertex, "shader.vert");
  loadShaderFromResources(QOpenGLShader::Fragment, "shader.frag");
}

void
ShaderProgram::link()
{
  if (!shaderProgram->link()) {
    throw std::runtime_error("Shader not linked");
  }
}

GLuint
ShaderProgram::id()
{
  return shaderProgram->programId();
}

void
ShaderProgram::deleteAllShaders()
{
  shaderProgram->removeAllShaders();
}

void
ShaderProgram::loadShaderFromResources(const QOpenGLShader::ShaderTypeBit& type,
                                       const QString& filename)
{
  bool loaded{ shaderProgram->addShaderFromSourceFile(
    type, QString(":/%1").arg(filename)) };
  if (!loaded) {
    throw std::runtime_error(
      QString("Could not load the shader '%1'").arg(filename).toStdString());
  }
}
