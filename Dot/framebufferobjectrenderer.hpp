#ifndef FRAMEBUFFEROBJECTRENDERER_H
#define FRAMEBUFFEROBJECTRENDERER_H

#include "dotrenderer.hpp"
#include <QQuickFramebufferObject>
#include <QQuickWindow>

class FrameBufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
public:
  FrameBufferObjectRenderer();
  void synchronize(QQuickFramebufferObject* item) override;
  void render() override;
  QOpenGLFramebufferObject* createFramebufferObject(const QSize& size) override;

private:
  QQuickWindow* quickWindow;
  DotRenderer dotRenderer;
};

#endif // FRAMEBUFFEROBJECTRENDERER_H
