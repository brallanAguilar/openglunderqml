#include "dotrenderer.hpp"
#include <QOpenGLContext>
#include <iostream>

DotRenderer::DotRenderer(QObject* parent)
  : QObject{ parent }
  , VERTICES{ { 0.0f, 0.0f, 0.0f } }
  , functions{ QOpenGLContext::currentContext()->functions() }
  , vbo{ QOpenGLBuffer::VertexBuffer }
{
}

DotRenderer::~DotRenderer()
{
  vbo.destroy();
  vao.destroy();
}

void
DotRenderer::initialize()
{
  try {
    initializeShaderProgram();
    initializeVao();
    initializeVbo();
  } catch (const std::runtime_error& ex) {
    std::cerr << ex.what() << "\nProgram was not initialized" << std::endl;
  }
  functions->glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
  functions->glClear(GL_COLOR_BUFFER_BIT);
}

void
DotRenderer::render()
{
  functions->glClearColor(0.9f, 0.9f, 0.9f, 1.0f);

  // Clear the color buffer
  functions->glClear(GL_COLOR_BUFFER_BIT);

  vao.bind();
  vbo.bind();

  // Use the program object
  functions->glUseProgram(shaderProgram.id());

  // Allows to use the gl_PointSize on the vertex shader
  functions->glEnable(GL_PROGRAM_POINT_SIZE);

  // 1st attribute buffer: vertices
  functions->glEnableVertexAttribArray(0);

  // Load the vertex position
  functions->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

  // Starting from vertex 0; 3 vertices total -> 1 triangle
  functions->glDrawArrays(GL_POINTS, 0, VERTICES.size());

  vao.release();
  vbo.release();
}

void
DotRenderer::initializeShaderProgram()
{
  shaderProgram.create();
  shaderProgram.loadShaders();
  shaderProgram.link();
  shaderProgram.deleteAllShaders();
}

void
DotRenderer::initializeVao()
{
  vao.create();
  vao.bind();
  vao.release();
}

void
DotRenderer::initializeVbo()
{
  vbo.create();
  vbo.bind();
  vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
  vbo.allocate(VERTICES.constData(), sizeof(QVector3D) * VERTICES.size());
  vbo.release();
}
