import qbs

Product {
    type: ["application"]
    cpp.cxxLanguageVersion: "c++17"
    Depends {
        name: "Qt"
        submodules: ["core", "quick", "opengl", "qml"]
    }
    files: [
        "OpenGLCanvas.qml",
        "Panel.qml",
        "framebufferobject.cpp",
        "framebufferobject.hpp",
        "framebufferobjectrenderer.cpp",
        "framebufferobjectrenderer.hpp",
        "main.cpp",
        "main.qml",
        "point.frag",
        "point.vert",
        "qml.qrc",
        "triangle.frag",
        "triangle.vert",
        "trianglerenderer.cpp",
        "trianglerenderer.hpp",
    ]
}
