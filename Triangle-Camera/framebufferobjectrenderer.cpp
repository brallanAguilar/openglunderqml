#include "framebufferobjectrenderer.hpp"
#include "framebufferobject.hpp"
#include <QOpenGLFramebufferObjectFormat>

FrameBufferObjectRenderer::FrameBufferObjectRenderer()
  : rotation{ { -1.0f, -1.0f }, false }
  , spherical{ { -1.0f, -1.0f, -1.0f }, false }
  , scale{ -1.0f, false }
{
  dotRenderer.initialize();
}

void
FrameBufferObjectRenderer::synchronize(QQuickFramebufferObject* item)
{
  updateRenderer(item);
  unprojectMouseCoordinates(item);
  quickWindow = item->window();
}

void
FrameBufferObjectRenderer::render()
{
  dotRenderer.render(rotation.second || spherical.second || scale.second,
                     pointAdded);
  quickWindow->resetOpenGLState();
}

QOpenGLFramebufferObject*
FrameBufferObjectRenderer::createFramebufferObject(const QSize& size)
{
  QOpenGLFramebufferObjectFormat format;
  format.setSamples(4);
  format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
  dotRenderer.setViewportSize(size);
  rotation.second = true;
  return new QOpenGLFramebufferObject(size, format);
}

void
FrameBufferObjectRenderer::checkPointAdded(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  pointAdded = fbo->hasPoints();
  if (pointAdded) {
    QVector3D pointClicked{ fbo->pointClicked() };
    pointClicked.setZ(TriangleCameraRenderer::TRIANGLE_Z - 0.01f);
    dotRenderer.appendVertex(pointClicked);
    fbo->pointProcessed();
  }
}

void
FrameBufferObjectRenderer::scaleChanged(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  scale.second = fbo->scalation() != scale.first;
  if (scale.second) {
    dotRenderer.setScale(fbo->scalation());
    scale.first = fbo->scalation();
  }
}

void
FrameBufferObjectRenderer::rotationChanged(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  rotation.second = fbo->rotation() != rotation.first;
  if (rotation.second) {
    dotRenderer.setRotation(fbo->rotation());
    rotation.first = fbo->rotation();
  }
}

void
FrameBufferObjectRenderer::sphericalChanged(QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  spherical.second = fbo->spherical() != spherical.first;
  if (spherical.second) {
    dotRenderer.setCylindrical(fbo->spherical());
    spherical.first = fbo->spherical();
  }
}

void
FrameBufferObjectRenderer::updateRenderer(QQuickFramebufferObject* item)
{
  checkPointAdded(item);
  rotationChanged(item);
  sphericalChanged(item);
  scaleChanged(item);
}

void
FrameBufferObjectRenderer::unprojectMouseCoordinates(
  QQuickFramebufferObject* item)
{
  FrameBufferObject* fbo{ static_cast<FrameBufferObject*>(item) };
  QVector3D newCoordinates{ dotRenderer.unprojectPoint(
    fbo->getUnprojectPoint()) };
  fbo->setUnprojectPoint(newCoordinates);
}
