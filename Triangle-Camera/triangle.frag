#version 330 core

in vec4 light;
out vec4 color;

void main()
{
  color = light;
}
