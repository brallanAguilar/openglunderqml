#include "trianglerenderer.hpp"
#include <QOpenGLContext>
#include <QtMath>
#include <iostream>

TriangleCameraRenderer::TriangleCameraRenderer(QObject* parent)
  : QObject{ parent }
  , TRIANGLE_VERTICES{ { -1.0f, -1.0f, TRIANGLE_Z },
                       { 1.0f, -1.0f, TRIANGLE_Z },
                       { 0.0f, 1.0f, TRIANGLE_Z } }
  , functions{ QOpenGLContext::currentContext()->functions() }
  , triangleBuffer{ QOpenGLBuffer::VertexBuffer }
{
}

TriangleCameraRenderer::~TriangleCameraRenderer()
{
  triangleBuffer.destroy();
  pointBuffer.destroy();
  vao.destroy();
}

void
TriangleCameraRenderer::initialize()
{
  initializePrograms();
  initializeVao();
  initializeAllVbo();

  // Allows to use the gl_PointSize on the vertex shader
  functions->glEnable(GL_PROGRAM_POINT_SIZE);
}

void
TriangleCameraRenderer::initializePrograms()
{
  initializeShaderProgram(triangleShaderProgram, "triangle");
  initializeShaderProgram(pointShaderProgram, "point");
}

void
TriangleCameraRenderer::render(bool transformationDone, bool pointAdded)
{
  if (transformationDone)
    renderTriangles();
  renderMousePoints(pointAdded);
}

void
TriangleCameraRenderer::renderTriangles()
{
  clearCanvas();
  initializeMatrices();
  bindResources(triangleShaderProgram, triangleBuffer);
  triangleShaderProgram.setUniformValue("mvp", mvp);
  int vertexLocation{ useVertexAttributeLocation(triangleShaderProgram,
                                                 "position") };
  functions->glDrawArrays(GL_TRIANGLES, 0, TRIANGLE_VERTICES.size());
  releaseResources(triangleShaderProgram, triangleBuffer, vertexLocation);
}

void
TriangleCameraRenderer::renderMousePoints(bool pointAdded)
{
  bindResources(pointShaderProgram, pointBuffer);
  pointShaderProgram.setUniformValue("mvp", mvp);
  if (pointAdded)
    pointBuffer.allocate(mouseVertices.constData(),
                         sizeof(QVector3D) * mouseVertices.size());
  int vertexLocation{ useVertexAttributeLocation(pointShaderProgram,
                                                 "position") };
  functions->glDrawArrays(GL_POINTS, 0, mouseVertices.size());
  releaseResources(pointShaderProgram, pointBuffer, vertexLocation);
}

void
TriangleCameraRenderer::clearCanvas()
{
  // Set black background
  functions->glClearColor(0.8f, 0.3f, 0.5f, 1.0f);

  // Clear the color, depth and stencil buffer
  functions->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                     GL_STENCIL_BUFFER_BIT);
}

void
TriangleCameraRenderer::bindResources(QOpenGLShaderProgram& shaderProgram,
                                      QOpenGLBuffer& buffer)
{
  // Binds this shader program to the active QOpenGLContext and makes it the
  // current shader program
  shaderProgram.bind();

  // Binds this vertex array object to the OpenGL binding point. From this point
  // on and until release() is called or another vertex array object is bound,
  // any modifications made to vertex data state are stored inside this vertex
  // array object.
  // If another vertex array object is then bound you can later restore the set
  // of state associated with this object by calling bind() on this object once
  // again. This allows efficient changes between vertex data states in
  // rendering functions
  vao.bind();

  // Tell OpenGL which VBO to use
  buffer.bind();
}

void
TriangleCameraRenderer::releaseResources(QOpenGLShaderProgram& shaderProgram,
                                         QOpenGLBuffer& buffer,
                                         const int& vertexLocation)
{
  // It is good practice to disable each vertex attribute when it is not
  // immediately used. Leaving it enabled when a shader is not using it is a
  // sure way of asking for trouble
  shaderProgram.disableAttributeArray(vertexLocation);

  buffer.release();
  vao.release();
  shaderProgram.release();
}

int
TriangleCameraRenderer::useVertexAttributeLocation(
  QOpenGLShaderProgram& shaderProgram,
  const QString& locationName)
{
  // Tell OpenGL programmable pipeline how to locate vertex position data
  int vertexLocation{ shaderProgram.attributeLocation(locationName) };
  shaderProgram.enableAttributeArray(vertexLocation);
  shaderProgram.setAttributeBuffer(
    vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));
  return vertexLocation;
}

QVector3D
TriangleCameraRenderer::unprojectPoint(const QVector3D& vector)
{
  QMatrix4x4 C{ mvp(0, 0), mvp(0, 1), 0,     0.0f,
                mvp(1, 0), mvp(1, 1), 0,     0.0f,
                mvp(2, 0), mvp(2, 1), -1.0f, 0.0f,
                0.0f,      0.0f,      0.0f,  1.0f };
  QVector3D l{ vector.x() - mvp(0, 2) * TRIANGLE_Z,
               vector.y() - mvp(1, 2) * TRIANGLE_Z,
               -mvp(3, 0) * TRIANGLE_Z };
  QVector3D mouseInViewport{ C.inverted() * l };
  return mouseInViewport;
}

void
TriangleCameraRenderer::appendVertex(const QVector3D& point)
{
  mouseVertices.append(point);
}

void
TriangleCameraRenderer::setViewportSize(const QSize& size)
{
  viewportSize = size;
}

void
TriangleCameraRenderer::setCylindrical(const QVector3D cylindricalCoords)
{
  cylindrical = cylindricalCoords;
}

void
TriangleCameraRenderer::setRotation(const QVector2D angle)
{
  rotation = angle;
}

void
TriangleCameraRenderer::setScale(float scale)
{
  this->scale = scale;
}

void
TriangleCameraRenderer::initializeShaderProgram(
  QOpenGLShaderProgram& shaderProgram,
  const QString& filename)
{
  shaderProgram.create();
  shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                        QString(":/%1.vert").arg(filename));
  shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                        QString(":/%1.frag").arg(filename));
  shaderProgram.link();
  shaderProgram.bind();
}

void
TriangleCameraRenderer::initializeVao()
{
  vao.create();
  vao.bind();
  vao.release();
}

void
TriangleCameraRenderer::initializeVbo(QOpenGLBuffer& buffer,
                                      const QVector<QVector3D>& vertices,
                                      QOpenGLBuffer::UsagePattern pattern)
{
  buffer.create();
  buffer.bind();
  buffer.setUsagePattern(pattern);
  buffer.allocate(vertices.constData(), sizeof(QVector3D) * vertices.size());
  buffer.release();
}

void
TriangleCameraRenderer::initializeAllVbo()
{
  initializeVbo(triangleBuffer, TRIANGLE_VERTICES, QOpenGLBuffer::StaticDraw);
  initializeVbo(pointBuffer, mouseVertices, QOpenGLBuffer::DynamicDraw);
}

void
TriangleCameraRenderer::initializeMatrices()
{
  setModelMatrix();
  setViewMatrix();
  setProjectionMatrix();
  setModelViewProjectionMatrix();
}

void
TriangleCameraRenderer::setModelMatrix()
{
  model.setToIdentity();
  model.rotate(rotation.x(), 1.0f, 0.0f);
  model.rotate(rotation.y(), 0.0f, 1.0f);
  model.scale(scale);
}

void
TriangleCameraRenderer::setViewMatrix()
{
  view.setToIdentity();
  const QVector3D eye{ sphericalCoords() };
  const QVector3D center{ 0.0f, 0.0f, 0.0f }; // Where the camera is looking at
  const QVector3D up{ 0.0f,
                      1.0f,
                      0.0f }; // Head is up (set to 0,-1,0 to look upside-down)
  view.lookAt(eye, center, up);
}

QVector3D
TriangleCameraRenderer::sphericalCoords()
{
  const float r{ cylindrical.x() }; // distance to origin

  // angle up from x-z plane, clamped to [0:Pi/2]
  const float phi{ qDegreesToRadians(cylindrical.z()) };

  // angle around y-axis, as measured from positive x-axis
  const float theta{ qDegreesToRadians(cylindrical.y()) };

  const float x{ r * std::cos(theta) * std::sin(phi) };
  const float y{ r * std::sin(theta) * std::sin(phi) };
  const float z{ r * std::cos(phi) };
  return QVector3D{ x, y, z };
}

void
TriangleCameraRenderer::setProjectionMatrix()
{
  projection.setToIdentity();
  const float aspectRatio{ static_cast<float>(viewportSize.width()) /
                           static_cast<float>(viewportSize.height()) };
  projection.perspective(qDegreesToRadians(0.0f), aspectRatio, 0.1f, 100.0f);
  // Or, for an ortho camera :
  //  projection.ortho(
  //    -1.0f, 1.0f, -1.0f, 1.0f, -5.0f, 5.0f); // In world coordinates
}

void
TriangleCameraRenderer::setModelViewProjectionMatrix()
{
  mvp = projection * view * model;
}
