#ifndef FRAMEBUFFEROBJECTRENDERER_HPP
#define FRAMEBUFFEROBJECTRENDERER_HPP

#include "framebufferobject.hpp"
#include "trianglerenderer.hpp"
#include <QQuickFramebufferObject>
#include <QQuickWindow>

class FrameBufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
public:
  FrameBufferObjectRenderer();
  void synchronize(QQuickFramebufferObject* item) override;
  void render() override;
  QOpenGLFramebufferObject* createFramebufferObject(const QSize& size) override;

private:
  QQuickWindow* quickWindow;
  TriangleCameraRenderer dotRenderer;
  void updateRenderer(QQuickFramebufferObject* item);

  /**
   * @brief rotationY value of rotationY and if it is different to a previous
   * value
   */
  QPair<QVector2D, bool> rotation;

  QPair<QVector3D, bool> spherical;
  QPair<float, bool> scale;
  bool pointAdded;
  void checkPointAdded(QQuickFramebufferObject* item);
  void scaleChanged(QQuickFramebufferObject* item);
  void rotationChanged(QQuickFramebufferObject* item);
  void sphericalChanged(QQuickFramebufferObject* item);
  void unprojectMouseCoordinates(QQuickFramebufferObject* item);
};

#endif // FRAMEBUFFEROBJECTRENDERER_HPP
