#version 330 core

// Attribute
in vec3 position;

// Model-view projection matrix
uniform mat4 mvp;

void main()
{
  // write the size of the point sprite in pixels
  gl_PointSize = 4.0;
  gl_Position = mvp * vec4(position, 1.0);
}
