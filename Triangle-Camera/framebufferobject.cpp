#include "framebufferobject.hpp"

FrameBufferObject::FrameBufferObject(QQuickItem* parent)
  : QQuickFramebufferObject(parent)
{
  setMirrorVertically(true);
}

void
FrameBufferObject::addPoint()
{
  m_pointClicked.first = m_mousePosition;
  m_pointClicked.second = true;
  update();
}

void
FrameBufferObject::unprojectPosition(QVector2D position)
{
  m_unprojectPoint = position;
  update();
}

QQuickFramebufferObject::Renderer*
FrameBufferObject::createRenderer() const
{
  return new FrameBufferObjectRenderer();
}

const QVector3D&
FrameBufferObject::pointClicked()
{
  return m_pointClicked.first;
}

bool
FrameBufferObject::hasPoints() const
{
  return m_pointClicked.second;
}

void
FrameBufferObject::pointProcessed()
{
  m_pointClicked.second = false;
}

void
FrameBufferObject::setUnprojectPoint(QVector3D point)
{
  m_mousePosition = point;
}

const QVector3D&
FrameBufferObject::getUnprojectPoint() const
{
  return m_unprojectPoint;
}

float
FrameBufferObject::scalation() const
{
  return m_scalation;
}

const QVector3D&
FrameBufferObject::spherical() const
{
  return m_spherical;
}

const QVector2D&
FrameBufferObject::rotation() const
{
  return m_rotation;
}

const QVector3D&
FrameBufferObject::mousePosition() const
{
  return m_mousePosition;
}

void
FrameBufferObject::setScalation(const float scale)
{
  if (m_scalation != scale) {
    m_scalation = scale;
    emit scalationChanged(scale);
    update();
  }
}

void
FrameBufferObject::setSpherical(const QVector3D& cylindrical)
{
  if (m_spherical != cylindrical) {
    m_spherical = cylindrical;
    emit sphericalChanged(cylindrical);
    update();
  }
}

void
FrameBufferObject::setRotation(const QVector2D& rotation)
{
  if (m_rotation != rotation) {
    m_rotation = rotation;
    emit rotationChanged(rotation);
    update();
  }
}

void
FrameBufferObject::setMousePosition(const QVector3D& position)
{
  if (m_mousePosition != position) {
    m_mousePosition = position;
    emit mousePositionChanged(position);
    update();
  }
}
