#include "framebufferobject.hpp"
#include <QDebug>
#include <QGuiApplication>
#include <QMatrix4x4>
#include <QQmlApplicationEngine>
#include <QSurfaceFormat>
#include <QVector3D>

void
setOpenGL()
{
  QSurfaceFormat format;

  // Use OpenGL 3.3
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setVersion(3, 3);
  format.setProfile(QSurfaceFormat::CoreProfile);

  format.setDepthBufferSize(12);
  format.setStencilBufferSize(8);
  format.setSamples(4);
  QSurfaceFormat::setDefaultFormat(format);
}

int
main(int argc, char* argv[])
{
  setOpenGL();
#if defined(Q_OS_WIN)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
  QGuiApplication app(argc, argv);

  qmlRegisterType<FrameBufferObject>("FrameBufferObject", 1, 0, "FbItem");

  QQmlApplicationEngine engine;
  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
