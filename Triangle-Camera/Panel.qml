import QtQuick.Controls 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.3

Item {
  property vector2d rotationScale: "0,0"
  property vector3d sphericalScale: "1,0,0"
  property real scalation: 1.0

  GridLayout {
    columns: 2
    Label {
      text: "Scale"
    }
    Slider {
      id: sliderScale
      from: 0.1
      to: 1.0
      value: 1.0
      onMoved: {
        scalation = value
      }
    }
    Label {
      text: "r"
    }
    Slider {
      id: sliderR
      from: 1
      to: 2
      value: 1
      onMoved: {
        sphericalScale.x = value
      }
    }
    Label {
      text: "θ"
    }
    Slider {
      id: sliderTheta
      from: 0
      to: 360
      value: 0
      onMoved: {
        sphericalScale.y = value
      }
    }
    Label {
      text: "ɸ"
    }
    Slider {
      id: sliderPhi
      from: 0
      to: 180
      value: 0
      onMoved: {
        sphericalScale.z = value
      }
    }
    Label {
      text: "Rotation X"
    }
    Slider {
      id: sliderRotX
      from: 0
      to: 180
      value: 0
      onMoved: {
        rotationScale.x = value
      }
    }
    Label {
      text: "Rotation Y"
    }
    Slider {
      id: sliderRotY
      from: 0
      to: 180
      value: 0
      onMoved: {
        rotationScale.y = value
      }
    }
    Button {
      text: "Reset"
      onClicked: {
        sliderScale.value = 1.0
        sliderR.value = 1.0
        sliderTheta.value = 0
        sliderPhi.value = 0
        sliderRotX.value = 0
        sliderRotY.value = 0
        sphericalScale = "1,0,0"
        rotationScale = "0,0"
        scalation = 1.0
      }
    }
  }
}
