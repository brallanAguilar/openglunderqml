#ifndef FRAMEBUFFEROBJECT_HPP
#define FRAMEBUFFEROBJECT_HPP

#include "framebufferobjectrenderer.hpp"
#include <QPair>
#include <QQuickFramebufferObject>
#include <QVector3D>

class FrameBufferObject : public QQuickFramebufferObject
{
  Q_OBJECT
  Q_PROPERTY(
    QVector2D rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
  Q_PROPERTY(
    float scalation READ scalation WRITE setScalation NOTIFY scalationChanged)
  Q_PROPERTY(QVector3D spherical READ spherical WRITE setSpherical NOTIFY
               sphericalChanged)
  Q_PROPERTY(QVector3D mousePosition READ mousePosition WRITE setMousePosition
               NOTIFY mousePositionChanged)

public:
  explicit FrameBufferObject(QQuickItem* parent = 0);
  Q_INVOKABLE void addPoint();
  Q_INVOKABLE void unprojectPosition(QVector2D position);
  Renderer* createRenderer() const override;
  const QVector3D& pointClicked();
  bool hasPoints() const;
  void pointProcessed();
  void setUnprojectPoint(QVector3D point);
  const QVector3D& getUnprojectPoint() const;
  float scalation() const;
  const QVector3D& spherical() const;
  const QVector2D& rotation() const;
  const QVector3D& mousePosition() const;

signals:
  void scalationChanged(float scale);
  void sphericalChanged(const QVector3D& spherical);
  void rotationChanged(const QVector2D& angle);
  void mousePositionChanged(const QVector3D& position);

public slots:
  void setScalation(const float scale);
  void setSpherical(const QVector3D& spherical);
  void setRotation(const QVector2D& rotation);
  void setMousePosition(const QVector3D& position);

private:
  float m_scalation;
  QVector3D m_spherical;
  QVector2D m_rotation;
  QVector3D m_mousePosition;
  QVector3D m_unprojectPoint;
  QPair<QVector3D, bool> m_pointClicked;
};

#endif // FRAMEBUFFEROBJECT_HPP
