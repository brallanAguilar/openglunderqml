import QtQuick.Controls 2.2
import QtQuick 2.9
import FrameBufferObject 1.0

Item {
  Rectangle {
    anchors.fill: parent
    anchors {
      horizontalCenter: parent.horizontalCenter
      verticalCenter: parent.verticalCenter
    }

    FbItem {
      id: fbo
      anchors.fill: parent
      rotation: panel.rotationScale
      spherical: panel.sphericalScale
      scalation: panel.scalation
      MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
          fbo.addPoint(mouseX, mouseY)
        }
        onPositionChanged: {
          var normalizedPoints = normalize(mouseX, mouseY)
          fbo.unprojectPosition(Qt.vector2d(normalizedPoints[0],
                                            normalizedPoints[1]))
          lblCoords.points = fbo.mousePosition
        }
        function normalize(px, py) {
          var limitX = fbo.width / 2
          var limitY = fbo.height / 2

          var cartX = (px - limitX) / limitX
          var cartY = -(py - limitY) / limitY

          return [cartX, cartY]
        }
      }
      Label {
        text: "Scale:" + fbo.formatNumber(fbo.scalation, 2)
        anchors.left: fbo.left
        anchors.top: fbo.up
        color: "white"
      }
      Label {
        text: "r:" + fbo.formatNumber(fbo.cylindrical.x, 2) + "  "
        anchors.right: lblPhi.left
        anchors.bottom: lblRotY.up
        color: "white"
      }
      Label {
        id: lblTheta
        text: "θ:" + fbo.formatNumber(fbo.cylindrical.y, 0) + "°"
        anchors.right: fbo.right
        anchors.bottom: lblRotY.up
        color: "white"
      }
      Label {
        id: lblPhi
        text: "ɸ:" + fbo.formatNumber(fbo.cylindrical.z, 0) + "°  "
        anchors.right: lblTheta.left
        anchors.bottom: lblRotY.up
        color: "white"
      }
      Label {
        text: "Rotation X:" + fbo.formatNumber(fbo.rotation.x, 0) + "°  "
        anchors.right: lblRotY.left
        anchors.bottom: fbo.bottom
        color: "white"
      }
      Label {
        id: lblRotY
        text: "Rotation Y:" + fbo.formatNumber(fbo.rotation.y, 0) + "°"
        anchors.right: fbo.right
        anchors.bottom: fbo.bottom
        color: "white"
      }
      Label {
        id: lblCoords
        property vector3d points
        text: "x:" + fbo.formatNumber(points.x, 3) + " y:" + fbo.formatNumber(
                points.y, 3) + " z:" + fbo.formatNumber(points.z, 3)
        anchors.left: fbo.left
        anchors.bottom: fbo.bottom
        color: "white"
      }
      function formatNumber(number, precision) {
        return number.toFixed(precision)
      }
    }
  }
}
