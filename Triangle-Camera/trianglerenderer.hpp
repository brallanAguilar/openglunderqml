#ifndef TRIANGLECAMERARENDERER_HPP
#define TRIANGLECAMERARENDERER_HPP

#include <QMatrix4x4>
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QVector3D>

class TriangleCameraRenderer : public QObject
{
  Q_OBJECT
public:
  // Figure's z coord where the cursor is
  static constexpr const float TRIANGLE_Z{ 0.5f };

  explicit TriangleCameraRenderer(QObject* parent = nullptr);
  ~TriangleCameraRenderer();

  void initialize();
  void render(bool transformationDone, bool pointAdded);
  void appendVertex(const QVector3D& point);
  void setViewportSize(const QSize& size);
  void setCylindrical(const QVector3D cylindricalCoords);
  void setRotation(const QVector2D angle);
  void setScale(float scale);

  /**
   * @brief unprojectPoint Equation system to obtain the unprojected x, y and
  z from vector
   * @param vector Projected vector on screen. It must provide their respective
  z coord when the object were not transformed
   * @return Mapped figure vector when all the matrices transformations are
  applied
   */
  QVector3D unprojectPoint(const QVector3D& vector);

private:
  const QVector<QVector3D> TRIANGLE_VERTICES;

  // ===========================================================================
  // OpenGL
  // ===========================================================================
  QOpenGLShaderProgram pointShaderProgram, triangleShaderProgram;
  QVector<QVector3D> mouseVertices;
  QOpenGLFunctions* functions;
  QOpenGLVertexArrayObject vao;
  QOpenGLBuffer triangleBuffer, pointBuffer;
  QSize viewportSize;
  // ===========================================================================

  // ===========================================================================
  // Matrix transformation
  // ===========================================================================
  QVector2D rotation;
  QVector3D cylindrical;
  float scale;

  QMatrix4x4 model;
  QMatrix4x4 view;
  QMatrix4x4 projection;
  QMatrix4x4 mvp;
  // ===========================================================================

  void initializeShaderProgram(QOpenGLShaderProgram& shaderProgram,
                               const QString& filename);
  void initializeVao();
  void initializeVbo(QOpenGLBuffer& buffer,
                     const QVector<QVector3D>& mouseVertices,
                     QOpenGLBuffer::UsagePattern pattern);
  void initializeMatrices();
  void setModelMatrix();
  void setViewMatrix();
  void setProjectionMatrix();
  void setModelViewProjectionMatrix();
  void initializeAllVbo();
  void bindResources(QOpenGLShaderProgram& shaderProgram,
                     QOpenGLBuffer& buffer);
  void releaseResources(QOpenGLShaderProgram& shaderProgram,
                        QOpenGLBuffer& buffer,
                        const int& vertexLocation);
  int useVertexAttributeLocation(QOpenGLShaderProgram& shaderProgram,
                                 const QString& locationName);
  void initializePrograms();
  void clearCanvas();
  void renderMousePoints(bool pointAdded);
  void renderTriangles();
  QVector3D sphericalCoords();
};

#endif // TRIANGLECAMERARENDERER_HPP
