import QtQuick 2.10
import QtQuick.Controls 2.2
import FrameBufferObject 1.0

ApplicationWindow {
  visible: true
  width: 640
  height: 480
  title: qsTr("Hello World OpenGL")

  FbItem {
    anchors.fill: parent
  }
}
