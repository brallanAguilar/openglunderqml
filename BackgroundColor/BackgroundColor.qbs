import qbs

Product {
    type: ["application"]
    cpp.cxxLanguageVersion: "c++17"
    Depends {
        name: "Qt"
        submodules: ["core", "quick", "opengl", "qml"]
    }
    files: [
        "backgroundrenderer.cpp",
        "backgroundrenderer.hpp",
        "framebufferobject.cpp",
        "framebufferobject.hpp",
        "framebufferobjectrenderer.cpp",
        "framebufferobjectrenderer.hpp",
        "main.cpp",
        "main.qml",
        "qml.qrc",
    ]
}
