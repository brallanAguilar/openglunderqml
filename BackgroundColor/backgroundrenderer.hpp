#ifndef BACKGROUNDRENDERER_HPP
#define BACKGROUNDRENDERER_HPP

#include <QObject>

class BackgroundRenderer : public QObject
{
  Q_OBJECT
public:
  explicit BackgroundRenderer(QObject* parent = nullptr);

  // All assume that the GL context is current.
  void render();
};

#endif // BACKGROUNDRENDERER_HPP
