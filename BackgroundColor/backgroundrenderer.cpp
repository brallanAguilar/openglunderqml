#include "backgroundrenderer.hpp"
#include <QOpenGLContext>
#include <QOpenGLFunctions>

BackgroundRenderer::BackgroundRenderer(QObject* parent)
  : QObject(parent)
{
}

void
BackgroundRenderer::render()
{
  QOpenGLFunctions* functions = QOpenGLContext::currentContext()->functions();
  functions->glClearColor(0.9f, 0.0f, 0.0f, 1.0f);
  functions->glClear(GL_COLOR_BUFFER_BIT);
}
