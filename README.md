# OpenGL examples for Qt 

## Requirements

- Qt 5.10.1
- GCC 7.3.1 or Clang 6.0.0
- OpenGL 3.1 support

## Description of each example

- **BackgroundColor**: set the FBO region color red
- **Dot**: add a black point in the middle of the window. The FBO region's background is gray 
- **DotClick**: maps screen coordinates to normalized device coordinates (NDC) to add a dot in the canvas. Also, allows to rotate the OpenGL world using a rotation matrix
- **Texture2D-2x2Image-GLES**: draw a rectangle with colored textures
- **Triangle-Camera**: use the concept of *camera* with both orthographic or perspective projections to change the point of view with spherical coordinates. This includes the mapping of NDC
- **Triangle-GL**: draw a red triangle at the FBO bottom using GLSL Core profile
- **Triangle-GLES**: draw a red triangle at a custom position (set with QML)