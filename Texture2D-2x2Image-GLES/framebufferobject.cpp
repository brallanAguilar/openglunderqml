#include "framebufferobject.hpp"

FrameBufferObject::FrameBufferObject(QQuickItem* parent)
  : QQuickFramebufferObject(parent)
{
}

QQuickFramebufferObject::Renderer*
FrameBufferObject::createRenderer() const
{
  return new FrameBufferObjectRenderer;
}
