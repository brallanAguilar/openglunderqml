#ifndef FRAMEBUFFEROBJECTRENDERER_H
#define FRAMEBUFFEROBJECTRENDERER_H

#include "imagerenderer.hpp"
#include <QQuickFramebufferObject>
#include <QQuickWindow>

class FrameBufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
public:
  FrameBufferObjectRenderer();
  void synchronize(QQuickFramebufferObject* item) override;
  void render() override;
  QOpenGLFramebufferObject* createFramebufferObject(const QSize& size) override;

private:
  QQuickWindow* quickWindow;
  ImageRenderer imageRenderer;
};

#endif // FRAMEBUFFEROBJECTRENDERER_H
