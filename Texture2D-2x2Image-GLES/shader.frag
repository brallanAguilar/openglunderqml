#version 310 es

// Based on Example 9-1 from Ginsburg and Budirijanto, "OpenGL ES 3.0
// Programming Guide", 3rd ed. New Jersey, USA: Pearson Education, Inc., 2009,
// p. 255

precision mediump float;

in vec2 v_texCoord;

// Render to a single color buffer, in which case the layout
// qualifier is optional (the output variable is assumed to go to location 0)
layout(location = 0) out vec4 outColor;
uniform sampler2D s_texture;

void main()
{
  outColor = texture(s_texture, v_texCoord);
}
