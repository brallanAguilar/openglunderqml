#include "framebufferobjectrenderer.hpp"
#include "framebufferobject.hpp"
#include <QOpenGLFramebufferObjectFormat>

FrameBufferObjectRenderer::FrameBufferObjectRenderer()
{
  imageRenderer.initialize();
}

void
FrameBufferObjectRenderer::synchronize(QQuickFramebufferObject* item)
{
  quickWindow = item->window();
}

void
FrameBufferObjectRenderer::render()
{
  imageRenderer.render();
  quickWindow->resetOpenGLState();
}

QOpenGLFramebufferObject*
FrameBufferObjectRenderer::createFramebufferObject(const QSize& size)
{
  QOpenGLFramebufferObjectFormat format;
  format.setSamples(4);
  format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
  return new QOpenGLFramebufferObject(size, format);
}
