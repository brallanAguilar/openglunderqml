import qbs

Product {
    type: ["application"]
    cpp.cxxLanguageVersion: "c++17"
    Depends {
        name: "Qt"
        submodules: ["core", "quick", "opengl", "qml"]
    }
    files: [
        "framebufferobject.cpp",
        "framebufferobject.hpp",
        "framebufferobjectrenderer.cpp",
        "framebufferobjectrenderer.hpp",
        "imagerenderer.cpp",
        "imagerenderer.hpp",
        "main.cpp",
        "main.qml",
        "qml.qrc",
        "shader.frag",
        "shader.vert",
        "shaderprogram.cpp",
        "shaderprogram.hpp",
    ]
}
