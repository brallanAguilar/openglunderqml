#include "imagerenderer.hpp"
#include <QOpenGLContext>
#include <iostream>

ImageRenderer::ImageRenderer(QObject* parent)
  : QObject{ parent }
  , WIDTH{ 2 }
  , HEIGHT{ 2 }
  , PIXELS{
      255,   0,   0, // Red
        0, 255,   0, // Green
        0,   0, 255, // Blue
      255, 255,   0, // Yellow
    }
  , V_VERTICES{
      -0.5f,   0.5f,  0.0f, // Position 0
       0.0f,   0.0f,        // TexCoord 0
      -0.5f,  -0.5f,  0.0f, // Position 1
       0.0f,   1.0f,        // TexCoord 1
       0.5f,  -0.5f,  0.0f, // Position 2
       1.0f,   1.0f,        // TexCoord 2
       0.5f,   0.5f,  0.0f, // Position 3
       1.0f,   0.0f         // TexCoord 3
    }
  , INDICES{ 0, 1, 2, 0, 2, 3 }
  , functions{ QOpenGLContext::currentContext()->functions() }
  , textureId{ 0 }
  , samplerLoc{ 0 }
{
}

void
ImageRenderer::initialize()
{
  try {
    initializeShaderProgram();

    // Get the sampler location
    samplerLoc =
      functions->glGetUniformLocation(shaderProgram.id(), "s_texture");

    // Load the texture
    textureId = createSimpleTexture2D();
  } catch (const std::runtime_error& ex) {
    std::cerr << ex.what() << "\nProgram was not initialized" << std::endl;
  }
}

void
ImageRenderer::render()
{
  // Set the viewport
  functions->glViewport(0, 0, 320, 240);

  // Clear the color buffer
  functions->glClear(GL_COLOR_BUFFER_BIT);

  // Use the program object
  functions->glUseProgram(shaderProgram.id());

  // Load the vertex position. The first argument corresponds to the 'location'
  // in shader.vert 'a_position' input variable
  functions->glVertexAttribPointer(
    0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), V_VERTICES);

  // Load the texture coordinate. The first argument corresponds to the
  // 'location' in shader.vert 'a_texCoord' input variable
  functions->glVertexAttribPointer(
    1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &V_VERTICES[3]);

  functions->glEnableVertexAttribArray(0);
  functions->glEnableVertexAttribArray(1);

  // Bind the texture
  functions->glActiveTexture(GL_TEXTURE0);
  functions->glBindTexture(GL_TEXTURE_2D, textureId);

  // Set the sampler texture unit to 0
  functions->glUniform1i(samplerLoc, 0);

  functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, INDICES);
}

GLuint
ImageRenderer::createSimpleTexture2D()
{
  GLuint textureId{ 0 };

  // Use tightly packed data
  functions->glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  // Generate a texture object
  functions->glGenTextures(1, &textureId);

  // Bind the texture object
  functions->glBindTexture(GL_TEXTURE_2D, textureId);

  // Load the texture
  functions->glTexImage2D(GL_TEXTURE_2D,
                          0,
                          GL_RGB,
                          WIDTH,
                          HEIGHT,
                          0,
                          GL_RGB,
                          GL_UNSIGNED_BYTE,
                          PIXELS);

  // Set the filtering mode
  functions->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  functions->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  return textureId;
}

void
ImageRenderer::initializeShaderProgram()
{
  shaderProgram.create();
  shaderProgram.loadShaders();
  shaderProgram.link();
}
