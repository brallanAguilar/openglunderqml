#ifndef IMAGERENDERER_HPP
#define IMAGERENDERER_HPP

#include "shaderprogram.hpp"
#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

/**
 * @brief The ImageRenderer class
 * @author Brallan Aguilar
 * @note Based on Example 2-1 and Example 9-1 from Ginsburg and Budirijanto,
 * "OpenGL ES 3.0 Programming Guide", 3rd ed. New Jersey, USA: Pearson
 * Education, Inc., 2009, pp. 29-32, 234
 */
class ImageRenderer : public QObject
{
  Q_OBJECT
public:
  explicit ImageRenderer(QObject* parent = nullptr);
  void initialize();
  void render();

private:
  const int WIDTH;
  const int HEIGHT;

  /**
   * @brief pixels 2 x 2 image, 3 bytes per pixel (R, G, B)
   */
  const GLubyte PIXELS[4 * 3];
  const GLfloat V_VERTICES[5 * 4];
  const GLushort INDICES[6];

  QOpenGLFunctions* functions;
  ShaderProgram shaderProgram;

  /**
   * @brief textureId Texture object handle
   */
  GLuint textureId;

  /**
   * @brief samplerLoc Sampler location
   */
  GLint samplerLoc;

  /**
   * @brief createSimpleTexture2D Create a simple 2x2 texture image with four
   * different colors
   */
  GLuint createSimpleTexture2D();

  /**
   * @brief initializeShaderProgram Load the shaders and get a linked program
   * object
   */
  void initializeShaderProgram();
};

#endif // IMAGERENDERER_HPP
