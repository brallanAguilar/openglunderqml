#include "framebufferobject.hpp"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSurfaceFormat>

void
setOpenGL()
{
  QSurfaceFormat format;

  // Use OpenGL ES 3.1
  format.setRenderableType(QSurfaceFormat::OpenGLES);
  format.setVersion(3, 1);
  format.setProfile(QSurfaceFormat::CoreProfile);

  format.setDepthBufferSize(24);
  format.setStencilBufferSize(8);
  format.setSamples(4);
  QSurfaceFormat::setDefaultFormat(format);
}

int
main(int argc, char* argv[])
{
#if defined(Q_OS_WIN)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QGuiApplication app(argc, argv);
  setOpenGL();

  qmlRegisterType<FrameBufferObject>("FrameBufferObject", 1, 0, "FbItem");

  QQmlApplicationEngine engine;
  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
