#version 310 es

// Based on Example 9-1 from Ginsburg and Budirijanto, "OpenGL ES 3.0
// Programming Guide", 3rd ed. New Jersey, USA: Pearson Education, Inc., 2009,
// p. 255

layout(location = 0) in vec4 a_position;
layout(location = 1) in vec2 a_texCoord;
out vec2 v_texCoord;

void main()
{
   gl_Position = a_position;
   v_texCoord = a_texCoord;
}
