#ifndef SHADERPROGRAM_HPP
#define SHADERPROGRAM_HPP

#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class QOpenGLShaderProgram;

class ShaderProgram
{
public:
  ShaderProgram();
  ~ShaderProgram();
  void create();

  /**
   * @brief loadShaders Load fragment and vertex shaders from QRC
   */
  void loadShaders();
  void link();
  GLuint id();
  void deleteAllShaders();

private:
  QOpenGLShaderProgram* shaderProgram;

  /**
   * @brief loadShaderFromResources Load a file from QML resources
   * @param type A vertex or fragment shader
   * @param filename File name
   */
  void loadShaderFromResources(const QOpenGLShader::ShaderTypeBit& type,
                               const QString& filename);
};

#endif // SHADERPROGRAM_HPP
