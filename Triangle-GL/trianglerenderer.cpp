#include "trianglerenderer.hpp"
#include <QOpenGLContext>
#include <iostream>

TriangleRenderer::TriangleRenderer(QObject* parent)
  : QObject{ parent }
  , VERTICES{ -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f }
  , functions{ QOpenGLContext::currentContext()->functions() }
{
}

TriangleRenderer::~TriangleRenderer()
{
  vbo.destroy();
  vao.destroy();
}

void
TriangleRenderer::initialize()
{
  try {
    initializeShaderProgram();
    initializeVao();
    initializeVbo();
  } catch (const std::runtime_error& ex) {
    std::cerr << ex.what() << "\nProgram was not initialized" << std::endl;
  }
  functions->glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
  functions->glClear(GL_COLOR_BUFFER_BIT);
}

void
TriangleRenderer::render()
{
  // Set the viewport
  functions->glViewport(0, 0, 320, 240);

  // Clear the color buffer
  functions->glClear(GL_COLOR_BUFFER_BIT);

  vao.bind();
  vbo.bind();

  // Use the program object
  functions->glUseProgram(shaderProgram.id());

  // 1st attribute buffer: vertices
  functions->glEnableVertexAttribArray(0);

  // Load the vertex position
  functions->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

  // Starting from vertex 0; 3 vertices total -> 1 triangle
  functions->glDrawArrays(GL_TRIANGLES, 0, 3);

  vao.release();
  vbo.release();
}

void
TriangleRenderer::initializeShaderProgram()
{
  shaderProgram.create();
  shaderProgram.loadShaders();
  shaderProgram.link();
  shaderProgram.deleteAllShaders();
}

void
TriangleRenderer::initializeVao()
{
  vao.create();
  vao.bind();
  vao.release();
}

void
TriangleRenderer::initializeVbo()
{
  vbo.create();
  vbo.bind();
  vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
  vbo.allocate(VERTICES, sizeof(VERTICES));
  vbo.release();
}
