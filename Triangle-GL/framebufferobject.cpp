#include "framebufferobject.hpp"

FrameBufferObject::FrameBufferObject(QQuickItem* parent)
  : QQuickFramebufferObject(parent)
{
  setMirrorVertically(true);
}

QQuickFramebufferObject::Renderer*
FrameBufferObject::createRenderer() const
{
  return new FrameBufferObjectRenderer;
}
