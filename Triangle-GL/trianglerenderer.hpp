#ifndef TRIANGLERENDERER_HPP
#define TRIANGLERENDERER_HPP

#include "shaderprogram.hpp"
#include <QObject>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

class QOpenGLBuffer;
class QOpenGLVertexArrayObject;

class TriangleRenderer : public QObject
{
  Q_OBJECT
public:
  explicit TriangleRenderer(QObject* parent = nullptr);
  ~TriangleRenderer();
  void initialize();
  void render();

private:
  const GLfloat VERTICES[9];
  QOpenGLFunctions* functions;
  ShaderProgram shaderProgram;
  QOpenGLVertexArrayObject vao;
  QOpenGLBuffer vbo;

  /**
   * @brief initializeShaderProgram Load the shaders and get a linked program
   * object
   */
  void initializeShaderProgram();

  void initializeVao();
  void initializeVbo();
};

#endif // TRIANGLERENDERER_HPP
